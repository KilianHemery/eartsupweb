		// Initialize and add the map
		function initMap() {
		  // The location of Uluru
		  var nantes = {lat: 47.211536, lng: -1.566272};
		  // The map, centered at Uluru
		  var map = new google.maps.Map(
		      document.getElementById('map'), {zoom: 15, center: nantes});
		  // The marker, positioned at Uluru
		  var marker = new google.maps.Marker({position: nantes, map: map});
		}